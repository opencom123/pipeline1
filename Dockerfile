# Mentionnez ci-dessous le dernier tag explicite disponible.
FROM httpd:2.4.54-bullseye
EXPOSE 80
WORKDIR /usr/local/apache2/htdocs
COPY . .
